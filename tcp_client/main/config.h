#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_spi_flash.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "protocol_examples_common.h"
#include "addr_from_stdin.h"
#include "driver/adc.h"
#include "esp_sleep.h"
#include "driver/uart.h"
#include "soc/uart_struct.h"

#define SAMPLES 2048            //Must be a power of 2
#define SAMPLES_2 1024            //Must be a power of 2
#define SAMPLING_FREQUENCY_KHZ 200  //total sampling time = SAMPLES/SAMPLING_FREQUENCY

static const int RX_BUF_SIZE = 16;

#define CE_PIN 25
#define CSN_PIN 26
#define IRQ_PIN 36

#define MSPI_HOST    VSPI_HOST
#define DMA_CHAN    0
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   26

#define DEFAULT_SSID "telenet-7741036"
#define DEFAULT_PWD "yk3dutPfm4pz"
#define DEFAULT_LISTEN_INTERVAL 100

esp_err_t ret;
spi_device_handle_t spi;

#endif