/*
 * File:   nrf24l01.c
 * Author: pic18fxx.blogspot.com
 */
#include <stdio.h>
#include "nrf24l01.h"
#include "config.h"

// This data type sets the address data pipe 0.

void send_cmd(spi_device_handle_t spi, const uint8_t cmd) 
{
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8;                     //Command is 8 bits
    t.tx_buffer=&cmd;               //The data is the cmd itself
    t.user=(void*)0;                //D/C needs to be set to 0
    ret=spi_device_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}

unsigned char SPI1_Recv8bit(){
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));
    t.length=8;   //the chip will have 12 usefull bit but read 16 because we need to have complete bytes!
    t.flags = SPI_TRANS_USE_RXDATA; //use only the received data!
    //t.tx_buffer=NULL;   //we don't need the tx buffer!
    esp_err_t ret = ret=spi_device_transmit(spi, &t);
    assert( ret == ESP_OK );
    printf("d0: %d, d1: %d, d2: %d, d3: %d",t.rx_data[0],t.rx_data[1],t.rx_data[2],t.rx_data[3]);
    return (t.rx_data[0]);
}

void NRF24L01_WriteRegister(unsigned char Mnemonic, unsigned char value){
NRF24L01_CSN_LOW;
send_cmd(spi, W_REGISTER | Mnemonic);
send_cmd(spi,value);
NRF24L01_CSN_HIGH;       
}

unsigned char NRF24L01_ReadRegister(unsigned char Mnemonic){
unsigned char byte0;
NRF24L01_CSN_LOW; 
send_cmd(spi,R_REGISTER | Mnemonic);
byte0 = SPI1_Recv8bit();
NRF24L01_CSN_HIGH; 
return byte0;
}

void NRF24L01_WriteBuffer(unsigned char data, unsigned char *buffer, unsigned char bytes){
unsigned char i;
NRF24L01_CSN_LOW;                                 
send_cmd(spi,data);                       
for(i = 0; i < bytes; i++)     
   {
    send_cmd(spi,*buffer);
    buffer++;
   }
NRF24L01_CSN_HIGH;
}

void NRF24L01_ReadBuffer(unsigned char data, unsigned char *buffer, unsigned char bytes){
unsigned char i;
NRF24L01_CSN_LOW;                                     
send_cmd(spi,data);                       
for(i = 0; i < bytes; i++)     
   {
    *buffer = SPI1_Recv8bit();
    buffer++;
   }
*buffer = NULL;
NRF24L01_CSN_HIGH; 
}

void NRF24L01_Init(unsigned char mode, unsigned char rf_channel,unsigned char *ADDRESS_DATA_PIPE0,unsigned char *ADDRESS_DATA_PIPE1){
gpio_pad_select_gpio(CE_PIN);
gpio_pad_select_gpio(CSN_PIN);
gpio_set_direction(CE_PIN, GPIO_MODE_OUTPUT);
gpio_set_direction(CSN_PIN, GPIO_MODE_OUTPUT);
NRF24L01_CSN_HIGH; 
NRF24L01_CE_LOW; 
vTaskDelay(100 / portTICK_PERIOD_MS); 
NRF24L01_WriteRegister(CONFIG, 0x0A);
vTaskDelay(10 / portTICK_PERIOD_MS);
NRF24L01_WriteRegister(EN_AA, 0x3F);
NRF24L01_WriteRegister(EN_RXADDR, 0x3F);
NRF24L01_WriteRegister(SETUP_AW, 0x03);
NRF24L01_WriteRegister(SETUP_RETR, 0x38);
NRF24L01_SetChannel(rf_channel);
NRF24L01_WriteRegister(RF_SETUP, 0x06);
//NRF24L01_WriteRegister(STATUSS, 0x70);
NRF24L01_WriteBuffer(W_REGISTER | RX_ADDR_P0, ADDRESS_DATA_PIPE0, 5);
NRF24L01_WriteBuffer(W_REGISTER | RX_ADDR_P1, ADDRESS_DATA_PIPE1, 5);
NRF24L01_WriteBuffer(W_REGISTER | TX_ADDR, ADDRESS_DATA_PIPE0, 5); 
NRF24L01_WriteRegister(RX_PW_P0, PAYLOAD_BYTES); 
NRF24L01_WriteRegister(RX_PW_P1, PAYLOAD_BYTES); 
//NRF24L01_Flush();
vTaskDelay(100 / portTICK_PERIOD_MS);
NRF24L01_SetMode(mode);
vTaskDelay(100 / portTICK_PERIOD_MS);
}

void NRF24L01_SetMode(unsigned char mode){
NRF24L01_Flush();
NRF24L01_WriteRegister(STATUSS, 0x70); // Clear STATUS.    
switch(mode)
      {
       case 1:
              NRF24L01_WriteRegister(CONFIG, 0x0F);  // RX Control
              NRF24L01_CE_HIGH;
       break;
       case 2:
              NRF24L01_WriteRegister(CONFIG, 0x0E);  // TX Control 
              NRF24L01_CE_LOW;
       break;
      }
}

void NRF24L01_SendData(unsigned char *buffer){
NRF24L01_SetMode(TX_MODE);    
NRF24L01_WriteBuffer(W_TX_PAYLOAD, buffer, PAYLOAD_BYTES);
NRF24L01_CE_HIGH; 
vTaskDelay(1 / portTICK_PERIOD_MS); 
NRF24L01_CE_LOW;
}

unsigned char NRF24L01_DataReady(void){
if((NRF24L01_ReadRegister(STATUSS) & 0x40) == 0x40)
  {
   return 1; 
  }
return 0;
}

void NRF24L01_ReadData(unsigned char *buffer){
NRF24L01_ReadBuffer(R_RX_PAYLOAD, buffer, PAYLOAD_BYTES);
NRF24L01_WriteRegister(STATUSS, 0x70); // Clear STATUS.
NRF24L01_Flush();
}

void NRF24L01_SetChannel(unsigned char rf_channel){
NRF24L01_WriteRegister(RF_CH, rf_channel);
}

unsigned char NRF24L01_GetChannel(void){
return NRF24L01_ReadRegister(RF_CH);
}

void NRF24L01_StandbyI(void){   
NRF24L01_WriteRegister(CONFIG, 0x0A);
vTaskDelay(10 / portTICK_PERIOD_MS); 
}

void NRF24L01_Flush(void){
NRF24L01_CSN_LOW; 
send_cmd(spi,FLUSH_TX);
NRF24L01_CSN_HIGH; 
NRF24L01_CSN_LOW; 
send_cmd(spi,FLUSH_RX);
NRF24L01_CSN_HIGH; 
}