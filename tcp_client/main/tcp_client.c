/* BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "config.h"
#include <math.h>
#include "handrail_bus.h"
#include "mirf.h"

#define BLINK_GPIO 2
#define ESP_INTR_FLAG_DEFAULT 0
#define MSPI_HOST    VSPI_HOST
#define DMA_CHAN    0
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18
#define PIN_NUM_CE1   16
#define PIN_NUM_CSN1  17
#define PIN_NUM_CE2   12
#define PIN_NUM_CSN2  4
#define IRQ1    22
#define IRQ2    14
#define SENSORID1 10
#define SENSORID2 20

#if defined(CONFIG_EXAMPLE_IPV4)
#define HOST_IP_ADDR CONFIG_EXAMPLE_IPV4_ADDR
#elif defined(CONFIG_EXAMPLE_IPV6)
#define HOST_IP_ADDR CONFIG_EXAMPLE_IPV6_ADDR
#else
#define HOST_IP_ADDR ""
#endif

#define PORT CONFIG_EXAMPLE_PORT

static const char *TAG = "example";
/*define the functions */

typedef struct {
    unsigned char buffer[3][6];  //buffer which contains the commands.
    unsigned char itemcounter;// variable that stores the no of itemsleft.
} tcprfbuffer;

typedef struct{
	uint8_t id;
	uint8_t command;
    uint32_t data;
} sensor_data_t;

tcprfbuffer bufferRFA[5];
tcprfbuffer bufferRFB[5];

unsigned char bufferRXARF[6] = {10,OK,1,2,3,4};
unsigned char bufferRXBRF[6] = {20,OK,1,2,3,4};
unsigned char bufferTX[6] = {1,1,1,1,1,1};
uint8_t somethingtosendA = 0;
uint8_t somethingtosendB = 0;
uint8_t somethingtosendAtoServer = 0;
uint8_t somethingtosendBtoServer = 0;
uint8_t payload = 6;
uint8_t channelA = 2;
uint8_t channelB = 3;
uint8_t txbuf [6] = {10,0,0,0,0,0};
int sock;
uint8_t sockok = 0;
NRF24_t devA,devB;
sensor_data_t RFA[3];
sensor_data_t RFB[3];
sensor_data_t tcprecv;

esp_err_t ret;
spi_device_handle_t spi;
spi_bus_config_t buscfg={
        .miso_io_num=PIN_NUM_MISO,
        .mosi_io_num=PIN_NUM_MOSI,
        .sclk_io_num=PIN_NUM_CLK,
        .quadwp_io_num=-1,
        .quadhd_io_num=-1,
        .max_transfer_sz=8
    };

spi_device_interface_config_t devcfg={
        .command_bits=0,
        .address_bits=0,
        .input_delay_ns=10, //as long as a bit receives within 10ns it's valid
        .dummy_bits=0,
        .cs_ena_pretrans=0,
        .cs_ena_posttrans=0,
        .clock_speed_hz= SPI_MASTER_FREQ_8M,           //Clock out at 8MHz
        .mode=0,                            //SPI mode 0
        .spics_io_num=-1,
        .duty_cycle_pos=128,                                     
        .queue_size=7,                          //We want to be able to queue 7 transactions at a time
    };

/*static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        esp_wifi_connect();
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip: " IPSTR, IP2STR(&event->ip_info.ip));
    }
}*/

static void rf_server_task_A(void *pvParameters)
{
    unsigned char activenodeA[3] = {0,0,0};
    unsigned char activenodeB[3] = {0,0,0};
    unsigned char activenodearrano = 0;
    for(uint8_t i = 0;i < 3;i++)
    {
    RFA[i].id = 0;
    RFA[i].command = 0;
    RFA[i].data = 0;
    RFB[i].id = 0;
    RFB[i].command = 0;
    RFB[i].data = 0;
    }

    Nrf24_powerUpRx(&devA);
    Nrf24_powerUpRx(&devB);

    while (1) 
    {
        while (Nrf24_dataReady(&devA)) 
        { //When the program is received, the received data is output from the serial port
			Nrf24_getData(&devA, bufferRXARF);
			ESP_LOGI(pcTaskGetTaskName(0), "Got data A:%d %d %d %d %d %d", bufferRXARF[0],bufferRXARF[1],bufferRXARF[2],bufferRXARF[3],bufferRXARF[4],bufferRXARF[5]);
            activenodeA[somethingtosendA] = bufferRXARF[0];
            RFA[somethingtosendA].id = bufferRXARF[0];
            RFA[somethingtosendA].command = bufferRXARF[1];
            RFA[somethingtosendA].data = (((uint32_t)bufferRXARF[2]) << 24) + (((uint32_t)bufferRXARF[3]) << 16) + (((uint32_t)bufferRXARF[4]) << 8) + ((uint32_t)bufferRXARF[5]);
            somethingtosendA++;
            somethingtosendAtoServer++;
            Nrf24_print_byte_register(&devA, "STATUS\t", STATUS, 1);
        }
        while (Nrf24_dataReady(&devB)) 
        { //When the program is received, the received data is output from the serial port
			Nrf24_getData(&devB, bufferRXBRF);
			ESP_LOGI(pcTaskGetTaskName(0), "Got data B:%d %d %d %d %d %d", bufferRXBRF[0],bufferRXBRF[1],bufferRXBRF[2],bufferRXBRF[3],bufferRXBRF[4],bufferRXBRF[5]);
            activenodeB[somethingtosendB] = bufferRXBRF[0];
            RFB[somethingtosendB].id = bufferRXBRF[0];
            RFB[somethingtosendB].command = bufferRXBRF[1];
            RFB[somethingtosendB].data = (((uint32_t)bufferRXBRF[2]) << 24) + (((uint32_t)bufferRXBRF[3]) << 16) + (((uint32_t)bufferRXBRF[4]) << 8) + ((uint32_t)bufferRXBRF[5]);
            somethingtosendB++;
            somethingtosendBtoServer++;
            Nrf24_print_byte_register(&devB, "STATUS\t", STATUS, 1);
        }
        while(somethingtosendA  != 0)
        {
            somethingtosendA--;
            switch(activenodeA[somethingtosendA])
            {
                case 11:
                    activenodearrano = 0;
                    Nrf24_setTADDR(&devA, (uint8_t *)"00011");
                    break;
                case 12:
                    activenodearrano = 1;
                    Nrf24_setTADDR(&devA, (uint8_t *)"00013");
                    break;
                case 13:
                    activenodearrano = 2;
                    Nrf24_setTADDR(&devA, (uint8_t *)"00015");
                    break;
                case 14:
                    activenodearrano = 3;
                    Nrf24_setTADDR(&devA, (uint8_t *)"00017");
                    break; 
                case 15:
                    activenodearrano = 4;
                    Nrf24_setTADDR(&devA, (uint8_t *)"00019");
                    break;
                default:
                        ESP_LOGI(TAG, "unknown node:");
                        activenodearrano = 10;
                        //somethingtosendA = 0;
            }
            if(activenodearrano < 5)
            {
                uint8_t retries = 10;
                while (bufferRFA[activenodearrano].itemcounter > 0 && retries != 0)
                {
                    Nrf24_send(&devA, *(bufferRFA[activenodearrano].buffer + ((bufferRFA[activenodearrano].itemcounter) - 1)));				   //Send instructions
                    ESP_LOGI(TAG, "send to sense %d %d %d %d %d %d:",bufferRFA[activenodearrano].buffer[(bufferRFA[activenodearrano].itemcounter - 1)][0],bufferRFA[activenodearrano].buffer[(bufferRFA[activenodearrano].itemcounter - 1)][1],bufferRFA[activenodearrano].buffer[(bufferRFA[activenodearrano].itemcounter - 1)][2],bufferRFA[activenodearrano].buffer[(bufferRFA[activenodearrano].itemcounter - 1)][3],bufferRFA[activenodearrano].buffer[(bufferRFA[activenodearrano].itemcounter - 1)][4],bufferRFA[activenodearrano].buffer[(bufferRFA[activenodearrano].itemcounter - 1)][5]);
		            if(Nrf24_isSend(&devA)){
                        bufferRFA[activenodearrano].itemcounter = bufferRFA[activenodearrano].itemcounter - 1;
                        //transfer was ok go to the next transfer
                        retries = 10;
                    }
                    else
                    {
                        vTaskDelay(10);
                        retries--;
                    }
                    
                    
                } 
                if(retries == 0)
                {
                    ESP_LOGI(TAG,"TRANSFER FAILED TO THE NODE! TRIES AGAIN WITH THE NEXT INCOMMING MESSAGE!");
                }
                activenodearrano = 20;
                Nrf24_isSend(&devA);
            }
        }
        while(somethingtosendB != 0)
        {
            somethingtosendB--;
            switch(activenodeB[somethingtosendA])
            {
                case 21:
                    activenodearrano = 0;
                    Nrf24_setTADDR(&devB, (uint8_t *)"00021");
                    break;
                case 22:
                    activenodearrano = 1;
                    Nrf24_setTADDR(&devB, (uint8_t *)"00023");
                    break;
                case 23:
                    activenodearrano = 2;
                    Nrf24_setTADDR(&devB, (uint8_t *)"00025");
                    break;
                case 24:
                    activenodearrano = 3;
                    Nrf24_setTADDR(&devB, (uint8_t *)"00027");
                    break;
                case 25:
                    activenodearrano = 4;
                    Nrf24_setTADDR(&devB, (uint8_t *)"00029");
                    break;
                default:
                    ESP_LOGI(TAG, "unknown node:");
                        activenodearrano = 10;
                        somethingtosendB = 0;
            }
            if(activenodearrano < 5)
            {
                while (bufferRFB[activenodearrano].itemcounter > 0)
                {
                    Nrf24_send(&devB, *(bufferRFB[activenodearrano].buffer + ((bufferRFB[activenodearrano].itemcounter) - 1)));				   //Send instructions
		            vTaskDelay(4);
                    bufferRFB[activenodearrano].itemcounter = bufferRFB[activenodearrano].itemcounter - 1;
                } 
                activenodearrano = 20;
                Nrf24_isSend(&devB);
            }
        }
        vTaskDelay(1);
        while(somethingtosendBtoServer != 0);
        while(somethingtosendAtoServer != 0);
    }
    vTaskDelete(NULL);
}

/*static void rf_server_task_B(void *pvParameters)
{
    unsigned char activenode = 0;
    unsigned char (*activebuffer)[6];
    unsigned char* itemsinbufferleft = &itemcounternode11;

    while (1) 
    {
        if (Nrf24_dataReady(&dev)) 
        { //When the program is received, the received data is output from the serial port
			Nrf24_getData(&dev, bufferRXBRF);
			ESP_LOGI(pcTaskGetTaskName(0), "Got data:%d %d %d %d %d %d", bufferRXBRF[0],bufferRXBRF[1],bufferRXBRF[2],bufferRXBRF[3],bufferRXBRF[4],bufferRXBRF[5]);
            somethingtosendB = 1;
            activenode = bufferRXBRF[0];
		}
        if(somethingtosendB)
        {
            switch(activenode)
            {
                case 21:
                    activebuffer = buffernode21;
                    itemsinbufferleft = &itemcounternode21;
                    Nrf24_setTADDR(&dev, (uint8_t *)"00021");
                    break;
                case 22:
                    activebuffer = buffernode22;
                    itemsinbufferleft = &itemcounternode22;
                    Nrf24_setTADDR(&dev, (uint8_t *)"00023");
                    break;
                case 23:
                    activebuffer = buffernode23;
                    itemsinbufferleft = &itemcounternode23;
                    Nrf24_setTADDR(&dev, (uint8_t *)"00025");
                    break;
                case 24:
                    activebuffer = buffernode24;
                    itemsinbufferleft = &itemcounternode24;
                    Nrf24_setTADDR(&dev, (uint8_t *)"00027");
                    break;
                case 25:
                    activebuffer = buffernode25;
                    itemsinbufferleft = &itemcounternode25;
                    Nrf24_setTADDR(&dev, (uint8_t *)"00029");
                    break;
                default:
                    ESP_LOGI(TAG, "unknown node:");
                    *itemsinbufferleft = 0; 
                    activebuffer = NULL;
            }
            while (*itemsinbufferleft > 0)
            {
                Nrf24_send(&dev, activebuffer[(*itemsinbufferleft - 1)]);				   //Send instructions
		        vTaskDelay(10);
                *itemsinbufferleft = *itemsinbufferleft - 1;
            }
            vTaskDelay(1);
        }
    }
    vTaskDelete(NULL);
}
*/

static void tcp_client_task(void *pvParameters)
{
    char host_ip[] = HOST_IP_ADDR;
    int addr_family = 0;
    int ip_protocol = 0;

    while (1) 
    {
    #if defined(CONFIG_EXAMPLE_IPV4)
        struct sockaddr_in dest_addr;
        dest_addr.sin_addr.s_addr = inet_addr(host_ip);
        dest_addr.sin_family = AF_INET;
        dest_addr.sin_port = htons(PORT);
        addr_family = AF_INET;
        ip_protocol = IPPROTO_IP;
    #elif defined(CONFIG_EXAMPLE_IPV6)
        struct sockaddr_in6 dest_addr = { 0 };
        inet6_aton(host_ip, &dest_addr.sin6_addr);
        dest_addr.sin6_family = AF_INET6;
        dest_addr.sin6_port = htons(PORT);
        dest_addr.sin6_scope_id = esp_netif_get_netif_impl_index(EXAMPLE_INTERFACE);
        addr_family = AF_INET6;
        ip_protocol = IPPROTO_IPV6;
    #elif defined(CONFIG_EXAMPLE_SOCKET_IP_INPUT_STDIN)
        struct sockaddr_in6 dest_addr = { 0 };
        ESP_ERROR_CHECK(get_addr_from_stdin(PORT, SOCK_STREAM, &ip_protocol, &addr_family, &dest_addr));
    #endif
        int sock =  socket(addr_family, SOCK_STREAM, ip_protocol);
        if (sock < 0) 
        {
            ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
            close(sock);
            sockok = 0;
        }
        ESP_LOGI(TAG, "Socket created, connecting to %s:%d", host_ip, PORT);

        int err = connect(sock, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in6));
        if (err != 0) 
        {
            ESP_LOGE(TAG, "Socket unable to connect: errno %d", errno);
            sockok = 0;
            //break;
        }
        else
        {
            gpio_set_level( BLINK_GPIO, 1); //indicate that the connection was succesfull!
            sockok = 1;
        }
        ESP_LOGI(TAG, "Successfully connected");



        unsigned char rx_buffer[8];
        while (sockok) 
        {
            while(somethingtosendAtoServer != 0)
            {
            somethingtosendAtoServer--;
            int err = send(sock, (void*)&RFA[somethingtosendAtoServer], sizeof(RFA[0]), 0);
                if (err < 0) {
                     ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
                   break;
                }
            }
            while(somethingtosendBtoServer != 0)
            {
            somethingtosendBtoServer--;
            int err = send(sock, (void*)&RFB[somethingtosendBtoServer], sizeof(RFB[0]), 0);
                if (err < 0) {
                     ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
                   break;
                }
            }

            int len = recv(sock, (void*)&tcprecv, sizeof(tcprecv), MSG_DONTWAIT);
            // Error occurred during receiving
            if (len < 0 && errno != 11) 
            {
                ESP_LOGE(TAG, "recv failed: errno %d", errno);
                break;
            }
           // Data received
            else if(len > 0)
            {
                rx_buffer[0] = tcprecv.id;
                rx_buffer[1] = tcprecv.command;
                rx_buffer[2] = (uint8_t)(tcprecv.data >> 24);
                rx_buffer[3] = (uint8_t)(tcprecv.data >> 16);
                rx_buffer[4] = (uint8_t)(tcprecv.data >> 8);
                rx_buffer[5] = (uint8_t)(tcprecv.data);
                ESP_LOGI(TAG, "Received %d bytes %d %d %d %d %d %d:", len,rx_buffer[0],rx_buffer[1],rx_buffer[2],rx_buffer[3],rx_buffer[4],rx_buffer[5]);
                switch(rx_buffer[0])
                {
                    case 11:
                        memcpy(*(bufferRFA[0].buffer + bufferRFA[0].itemcounter),rx_buffer,6);
                         ESP_LOGI(TAG, "wrote in reg %d %d %d %d %d %d:",bufferRFA[0].buffer[(bufferRFA[0].itemcounter)][0],bufferRFA[0].buffer[(bufferRFA[0].itemcounter)][1],bufferRFA[0].buffer[(bufferRFA[0].itemcounter)][2],bufferRFA[0].buffer[(bufferRFA[0].itemcounter)][3],bufferRFA[0].buffer[(bufferRFA[0].itemcounter)][4],bufferRFA[0].buffer[(bufferRFA[0].itemcounter)][5]);
                        bufferRFA[0].itemcounter++;
                        break;
                    case 12:
                        memcpy(*(bufferRFA[1].buffer + bufferRFA[1].itemcounter),rx_buffer,6);
                        bufferRFA[1].itemcounter++;
                        break;
                    case 13:
                        memcpy(*(bufferRFA[2].buffer + bufferRFA[2].itemcounter),rx_buffer,6);
                        bufferRFA[2].itemcounter++;
                        break;
                    case 14:
                        memcpy(*(bufferRFA[3].buffer + bufferRFA[3].itemcounter),rx_buffer,6);
                        bufferRFA[3].itemcounter++; 
                        break; 
                    case 15:
                        memcpy(*(bufferRFA[4].buffer + bufferRFA[4].itemcounter),rx_buffer,6);
                        bufferRFA[4].itemcounter++;
                        break;
                    case 21:
                        memcpy(*(bufferRFB[0].buffer + bufferRFB[0].itemcounter),rx_buffer,6);
                        bufferRFB[0].itemcounter++;
                        break;
                    case 22:
                        memcpy(*(bufferRFB[1].buffer + bufferRFB[1].itemcounter),rx_buffer,6);
                        bufferRFB[1].itemcounter++;
                        break;
                    case 23:
                        memcpy(*(bufferRFB[2].buffer + bufferRFB[2].itemcounter),rx_buffer,6);
                        bufferRFB[2].itemcounter++;
                        break;
                    case 24:
                        memcpy(*(bufferRFB[3].buffer + bufferRFB[3].itemcounter),rx_buffer,6);
                        bufferRFB[3].itemcounter++;
                        break;
                    case 25:
                        memcpy(*(bufferRFB[4].buffer + bufferRFB[4].itemcounter),rx_buffer,6);
                        bufferRFB[4].itemcounter++;
                        break;
                    default:
                        ESP_LOGI(TAG, "unknown node:");
                }
                
            }

            if(!somethingtosendA && !somethingtosendB)
            {
                vTaskDelay(10);
            }
        }   
        
        if (sock != -1) {
            ESP_LOGE(TAG, "Shutting down socket and restarting...");
            gpio_set_level( BLINK_GPIO, 0); //indicate that the connection was lost!
            sockok = 0;
            shutdown(sock, 0);
            close(sock);
            vTaskDelay(3000);
        }
    
    }

    vTaskDelete(NULL);
}
void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    //check_efuse();

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    
    //esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    //assert(sta_netif);

    //wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    //ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    //ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, NULL));
    //ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL, NULL));

    /*wifi_config_t wifi_config = {
        .sta = {
            .ssid = DEFAULT_SSID,
            .password = DEFAULT_PWD,
            .listen_interval = DEFAULT_LISTEN_INTERVAL,
        },
    };*/
    //ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    //ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    //ESP_ERROR_CHECK(esp_wifi_start());

    ESP_ERROR_CHECK(example_connect());

    printf("Booting Gateway\n");


    /* Print chip information */

    printf("intialize SPI");
    
    //Initialize the SPI bus
    //ret=spi_bus_initialize(MSPI_HOST, &buscfg, DMA_CHAN);
    //ESP_ERROR_CHECK(ret);
    //ret=spi_bus_add_device(MSPI_HOST, &devcfg, &spi);
    //ESP_ERROR_CHECK(ret);

    gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

    

    vTaskDelay(1000);

    gpio_pad_select_gpio(IRQ1);
    gpio_set_direction(IRQ1, GPIO_MODE_INPUT);

    /*config RF module A*/

	ESP_LOGI(pcTaskGetTaskName(0), "Start");
	ESP_LOGI(pcTaskGetTaskName(0), "CONFIG_CE_GPIO=%d",PIN_NUM_CE1);
	ESP_LOGI(pcTaskGetTaskName(0), "CONFIG_CSN_GPIO=%d",PIN_NUM_CSN1);
	spi_master_init(&devA, PIN_NUM_CE1, PIN_NUM_CSN1, PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK);
    spi_master_init_no_spi(&devB,&devA, PIN_NUM_CE2, PIN_NUM_CSN2, PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK);

	Nrf24_setRADDR(&devA, (uint8_t *)"00010");
	Nrf24_setTADDR(&devA, (uint8_t *)"00010");
	Nrf24_config(&devA, channelA, payload);

    Nrf24_setRADDR_other(&devA, (uint8_t *)"1",RX_ADDR_P2);
    Nrf24_configRegister(&devA, EN_RXADDR, 0x07);
    Nrf24_setRADDR_other(&devA, (uint8_t *)"2",RX_ADDR_P3);
    Nrf24_configRegister(&devA, EN_RXADDR, 0x0F);
    Nrf24_setRADDR_other(&devA, (uint8_t *)"3",RX_ADDR_P4);
    Nrf24_configRegister(&devA, EN_RXADDR, 0x1F);
    Nrf24_setRADDR_other(&devA, (uint8_t *)"4",RX_ADDR_P5);
    Nrf24_configRegister(&devA, EN_RXADDR, 0x3F);
    Nrf24_setTADDR(&devA, (uint8_t *)"00010");

    Nrf24_setRADDR(&devB, (uint8_t *)"00026");
    Nrf24_setRADDR_other(&devB, (uint8_t *)"2",RX_ADDR_P2);
    Nrf24_setRADDR_other(&devB, (uint8_t *)"4",RX_ADDR_P3);
    Nrf24_setRADDR_other(&devB, (uint8_t *)"0",RX_ADDR_P4);
    Nrf24_setRADDR_other(&devB, (uint8_t *)"8",RX_ADDR_P5);
	Nrf24_setTADDR(&devB, (uint8_t *)"00027");
	Nrf24_config(&devB, channelB, payload);


	Nrf24_printDetails(&devA);
	ESP_LOGI(pcTaskGetTaskName(0), "A_Listening...");

    /*config RF module B*/
    
	Nrf24_printDetails(&devB);
	ESP_LOGI(pcTaskGetTaskName(0), "B_Listening...");

    for(uint8_t counter = 0;counter < 5;counter++)
    {
        bufferRFA[counter].itemcounter = 0;
        bufferRFB[counter].itemcounter = 0;
    }

    //gpio_config_t io_conf;
    //interrupt of rising edge
    //io_conf.intr_type = GPIO_PIN_INTR_POSEDGE;
    //bit mask of the pins, use GPIO4/5 here
    //io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    //set as input mode    
    //io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    //io_conf.pull_up_en = 1;
    //gpio_config(&io_conf);

    //create a queue to handle gpio event from isr
    //gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

    //xTaskCreate(tcp_client_task, "tcp_client", 4096, NULL, 28, NULL); 
    xTaskCreate(rf_server_task_A, "rf_B", 4096, NULL, 10, NULL);
    xTaskCreate(tcp_client_task, "TCP", 4096, NULL, 9, NULL);  

    //install gpio isr service
    //gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    //gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void*) GPIO_INPUT_IO_0);
}
